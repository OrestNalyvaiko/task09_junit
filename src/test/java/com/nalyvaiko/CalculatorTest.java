package com.nalyvaiko;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.*;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {
    private final static boolean bug =  false;

    @InjectMocks
    private Calculator calc;

    @Mock
    private Substituter substituter;

    @Test
    public void addTwoValues2And4Return6() {
        int value = calc.addTwoValues(2, 4);
        assertEquals(value, 6, "Test failure,return value is not " + value);
    }

    @Test
    public void addTwoValues3And4Return7() {
        int value = calc.addTwoValues(3, 4);
        assertTrue(value == 7);
    }

    @Test
    public void divideTwoValuesBy0ThrowArithmeticException() {
        assertThrows(ArithmeticException.class, () -> calc.divideTwoValues(2, 0));
    }


    @Test
    public void addTwoValues2And3ReturnNotEquals6() {
        if (bug) {
            assumeTrue(false, "Test skipped due to not fixed bug");
        } else {
            int value = calc.addTwoValues(2, 3);
            assertNotEquals(value, 6);
        }
    }

    @Test
    public void subTwoValues20And10Return10() {
        when(substituter.subTwoValues(20, 10)).thenReturn(10);

        assertEquals(calc.subTwoValues(20,10),10);

        verify(substituter).subTwoValues(20, 10);
    }


}
