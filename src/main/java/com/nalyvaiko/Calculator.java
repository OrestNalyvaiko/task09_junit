package com.nalyvaiko;

public class Calculator {

    private Substituter substituter;
    public Calculator(){
        substituter = new Substituter();
    }
    public int addTwoValues(int firstValue,int secondValue){
        return firstValue + secondValue;
    }

    public double divideTwoValues(int firstValue,int secondValue) {
        if(secondValue != 0){
            return (double) firstValue/secondValue;
        } else {
            throw new ArithmeticException();
        }
    }

    public int subTwoValues(int firstValue,int secondValue){
        return substituter.subTwoValues(firstValue,secondValue);
    }
}
